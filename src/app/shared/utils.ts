/**
 * Created by Abbes on 30/06/2017.
 */
declare var jQuery: any;

export class Utils {


  public static zero(n: number) {
    if (n < 10) {
      return "0" + n;
    }
    return n;
  }


  private static groupBy(xs, key) {
    return xs.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  }


  public static groupByM(array, f) {
    let groups = {};
    array.forEach(function (o) {
      var group = JSON.stringify(f(o));
      groups[group] = groups[group] || [];
      groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
      return groups[group];
    })
  }


  static initializeDataTables(className, timout: number, columnNumber: number, orderBy ?: number) {
    console.log("init Datatable");
    // Basic datatable
    if (jQuery.fn.DataTable.isDataTable('.' + className)) {
      jQuery('.' + className).dataTable().fnDestroy();
    }
    setTimeout(function () {
        const tableListStation = jQuery('.' + className);
        // Setting datatable defaults
        jQuery.extend(jQuery.fn.dataTable.defaults, {
          autoWidth: false,
          columnDefs: [{
            orderable: false,
            width: '100px',
            targets: [columnNumber - 1]
          }],
          dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
          language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
          },
          drawCallback: function () {
            jQuery(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
          },
          preDrawCallback: function () {
            jQuery(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
          }
        });

        // console.log("order By " + orderBy);


        // Basic datatable
        tableListStation.DataTable({
          dom: 'Bfrtip',
          language: {
            "emptyTable": "..."
          },
          buttons: {
            dom: {
              button: {
                className: 'btn btn-default'
              }
            },
            buttons: [
              'excelHtml5',
              'pdfHtml5'
            ]
          }
          ,
          columnDefs: [{
            targets: [columnNumber - 1]
          }]
        });


        // Add placeholder to the datatable filter option
        jQuery('.dataTables_filter input[type=search]').attr('placeholder', 'Type to filter...');


        // Enable Select2 select for the length option
        jQuery('.dataTables_length select').select2({
          minimumResultsForSearch: Infinity,
          width: 'auto'
        });
      }
      , timout);


  }

  static convertDate(date: string) {
    console.log(date);
    if (date) {
      const HH = date.substring(11, 13);
      const mm = date.substring(14, 16);
      console.log(HH + ":" + mm);

      return date.substring(8, 10) + "/" + date.substring(5, 7) + "/" + date.substring(0, 4) + (HH && mm ? " " + HH + ":" + mm : "");
    }
    return null;
  }

  static convertTime(time: string) {
    if (time) {
      return time.substring(0, 5);
    }
    return null;
  }

  static convertRealDate(date: string, time?: string) {
    if (date) {
      console.log(date);

      console.log(date.substring(0, 2), date.substring(3, 5), date.substring(6, 10));
      if (!time) {
        return new Date(+date.substring(6, 10), (+date.substring(3, 5) - 1), +date.substring(0, 2));
      } else {
        return new Date(+date.substring(6, 10), (+date.substring(3, 5) - 1),
          +date.substring(0, 2), +time.substring(0, 2), +time.substring(3, 5));
      }
    }
    return null;
  }

  static getModalTemplate() {
    return '<div class="modal-dialog modal-lg" role="document">\n' +
      '  <div class="modal-content">\n' +
      '    <div class="modal-header">\n' +
      '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
      '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
      '    </div>\n' +
      '    <div class="modal-body">\n' +
      '      <div class="floating-buttons btn-group"></div>\n' +
      '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
      '    </div>\n' +
      '  </div>\n' +
      '</div>\n';
  }

  static getPreviewZoomButtonClasses() {
    return {
      toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
      fullscreen: 'btn btn-default btn-icon btn-xs',
      borderless: 'btn btn-default btn-icon btn-xs',
      close: 'btn btn-default btn-icon btn-xs'
    };
  }

  static getPreviewZoomButtonIcons() {
    return {
      prev: '<i class="icon-arrow-left32"></i>',
      next: '<i class="icon-arrow-right32"></i>',
      toggleheader: '<i class="icon-menu-open"></i>',
      fullscreen: '<i class="icon-screen-full"></i>',
      borderless: '<i class="icon-alignment-unalign"></i>',
      close: '<i class="icon-cross3"></i>'
    };
  }

  static initializeUploadFile(url: string, token: string, className: string,
                              showRemove: boolean,
                              showUpload: boolean,
                              maxFileCount?: number,
                              initialData?: any[],
                              initialPreviewConfig?: InitialPreviewConfig[]) {
    jQuery(className).fileinput({
      uploadUrl: url, // server upload action
      uploadAsync: true,
      showRemove: showRemove,
      showUpload: showUpload,
      maxFileCount: maxFileCount,
      overwriteInitial: false,
      initialPreview: initialData,
      initialPreviewAsData: true,
      initialPreviewFileType: 'image', // image is the default and can be overridden in config below
      initialPreviewConfig: initialPreviewConfig,
      fileActionSettings: {
        removeIcon: '<i class="icon-bin"></i>',
        removeClass: 'btn btn-link btn-xs btn-icon',
        uploadIcon: '<i class="icon-upload"></i>',
        uploadClass: 'btn btn-link btn-xs btn-icon',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        zoomClass: 'btn btn-link btn-xs btn-icon',
        downloadIcon: '<i class="icon-download"></i>',
        downloadClass: 'btn btn-link btn-xs btn-icon',
        indicatorNew: '<i class="icon-file-plus text-slate"></i>',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
      },
      purifyHtml: true, // this by default purifies HTML data for preview
      initialCaption: "Pas encore de fichier selectionné",
      previewZoomButtonClasses: Utils.getPreviewZoomButtonClasses(),
      previewZoomButtonIcons: Utils.getPreviewZoomButtonIcons(),
      ajaxSettings: {headers: {'Authorization': 'Bearer ' + token}},
    })
  }

  static convertDateServer(date: string) {
    const HH = date.substring(11, 13);
    const mm = date.substring(14, 16);
    return date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2) + (HH && mm ? " " + HH + ":" + mm : "");
  }

  static loadTypeFromExtension(ext: string) {
    if (ext.toLowerCase().match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i)) {
      return "video";
    }
    if (ext.toLowerCase().match(/(pdf)$/i)) {
      return "pdf";
    }
  }

  static loadFileTypeFromExtension(ext: string) {
    if (ext.toLowerCase().match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i)) {
      return "video/" + ext;
    }
    if (ext.toLowerCase().match(/(pdf)$/i)) {
      return "pdf";
    }
  }

  static getYears(minYear: number) {
    const years: number[] = [];

    for (let i = minYear; i <= new Date().getFullYear(); i++) {
      years.push(i);
    }
    return years;
  }

  static getUniversityYears(minYear: number) {
    const uniYears: string[] = [];

    for (let i = minYear; i <= new Date().getFullYear(); i++) {
      let j = i + 1;
      uniYears.push(i + "-" + j);
    }

    return uniYears;
  }

  static verifyNewStudent(study_access_year: string) {
    if (!study_access_year) {
      return false;
    }
    const year = parseInt(study_access_year.substring(0, 4));
    return year === new Date().getFullYear();
  }

  static getStatusSection(validations: any[], id_section: number) {

    if (!validations) {
      return null;
    }
    const result = jQuery.grep(validations, function (e) {
      return e.id_section === id_section;
    });


    if (!result || result.length === 0) {
      return null;
    } else {
      return result[0];
    }
  }


  static initSelect(className: string) {
    const select = jQuery('.' + className);
    select.select2();
  }

  static setValuesjQueryCmp(className: string, values: any, timout: number) {

    if (values) {
      setTimeout(function () {
        const component = jQuery('.' + className);
        component.val(values).trigger('change');
      }, timout);

    }
  }

  static getCurrentUniversityYear(cycle: number) {
    // cycle => 1 : PDCEM , 2 : IRESIDANT
    let fullYear = new Date().getFullYear();
    if (cycle === 1) {
      if (new Date().getMonth() >= 0 && new Date().getMonth() <= 5) {
        fullYear -= 1;
      }
    } else {
      if (new Date().getMonth() >= 0 && new Date().getMonth() <= 8) {
        fullYear -= 1;
      }
    }
    return (fullYear) + "-" + (fullYear + 1)
  }

  static getLabelNiveau(niveau_id: number) {
    switch (niveau_id) {
      case 1 :
        return "PCEM1";
      case 2 :
        return "PCEM2";
      case 3 :
        return "DCEM1";
      case 4 :
        return "DCEM2";
      case 5 :
        return "DCEM3";
      case 6 :
        return "DCEM4";
      case 7 :
        return "DCEM4 (AR)";
      case 8:
        return "TCEM1";
      case 9:
        return "TCEM2";
      case 10:
        return "TCEM3";
      case 11:
        return "TCEM4";
      case 12:
        return "TCEM5";

    }
  }


  static getLevelIdFromName(niveau_name: any) {
    switch (niveau_name) {
      case "PCEM1" :
        return 1;
      case "PCEM2" :
        return 2;
      case "DCEM1" :
        return 3;
      case "DCEM2" :
        return 4;
      case "DCEM3" :
        return 5;
      case "DCEM4" :
        return 6;
      case "DCEM4(AR)" :
        return 7;
      case "TCEM1" :
        return 8;
      case "TCEM2" :
        return 9;
      case "TCEM3" :
        return 10;
      case "TCEM4" :
        return 11;
      case "TCEM5" :
        return 12;
    }
  }

  static getResultIdFromName(result_name: any) {
    switch (result_name) {
      case "Redoublant" :
        return 1;
      case "Admis" :
        return 2;
      case "Admis avec crédit" :
        return 3;
      case "admis et orienté" :
        return 5;
    }
  }

  static getResultNameFromId(result_id: number) {
    switch (result_id) {
      case 1 :
        return "Redoublant";
      case  2:
        return "Admis";
      case 3:
        return "Admis avec crédit";
      case 5 :
        return "admis et orienté";
    }
  }


  static setValueToRadioBox(className: string, value: any, timout: number) {
    const radioBox = jQuery('.' + className).prop('checked', false);
    jQuery.uniform.update(radioBox);
    if (value !== undefined) {
      setTimeout(function () {
        const radioBox = jQuery('.' + className + '[value=' + value + ']').prop('checked', true);
        jQuery.uniform.update(radioBox);
      }, timout);
    }
  }

  static initDatePicker(className: string, timePicker: boolean) {
    jQuery('.' + className).daterangepicker({
      timePicker: timePicker,
      timePicker24Hour: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        format: 'DD/MM/YYYY HH:mm'
      }
    });

  }

  static getRandomString() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  static getIdentifiantStudent(student: any) {
    if (!student.cin && !student.passport) {
      return "-";
    }
    return student.cin ? student.cin.code : student.passport.code;
  }

  static initJsProject() {
    setTimeout(function () {


      // Allow CSS transitions when page is loaded
      jQuery(window).on('load', function () {
        jQuery('body').removeClass('no-transitions');
      });


      jQuery(function () {

        // Disable CSS transitions on page load
        jQuery('body').addClass('no-transitions');


        // ========================================
        //
        // Content area height
        //
        // ========================================


        // Calculate min height
        function containerHeight() {
          var availableHeight = jQuery(window).height() - jQuery('.page-container').offset().top - jQuery('.navbar-fixed-bottom').outerHeight();

          jQuery('.page-container').attr('style', 'min-height:' + availableHeight + 'px');
        }

        // Initialize
        containerHeight();


        // ========================================
        //
        // Heading elements
        //
        // ========================================


        // Heading elements toggler
        // -------------------------

        // Add control button toggler to page and panel headers if have heading elements
        jQuery('.panel-footer').has('> .heading-elements:not(.not-collapsible)').prepend('<a class="heading-elements-toggle"><i class="icon-more"></i></a>');
        jQuery('.page-title, .panel-title').parent().has('> .heading-elements:not(.not-collapsible)').children('.page-title, .panel-title').append('<a class="heading-elements-toggle"><i class="icon-more"></i></a>');


        // Toggle visible state of heading elements
        jQuery('.page-title .heading-elements-toggle, .panel-title .heading-elements-toggle').on('click', function () {
          jQuery(this).parent().parent().toggleClass('has-visible-elements').children('.heading-elements').toggleClass('visible-elements');
        });
        jQuery('.panel-footer .heading-elements-toggle').on('click', function () {
          jQuery(this).parent().toggleClass('has-visible-elements').children('.heading-elements').toggleClass('visible-elements');
        });


        // Breadcrumb elements toggler
        // -------------------------

        // Add control button toggler to breadcrumbs if has elements
        jQuery('.breadcrumb-line').has('.breadcrumb-elements').prepend('<a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>');


        // Toggle visible state of breadcrumb elements
        jQuery('.breadcrumb-elements-toggle').on('click', function () {
          jQuery(this).parent().children('.breadcrumb-elements').toggleClass('visible-elements');
        });


        // ========================================
        //
        // Navbar
        //
        // ========================================


        // Navbar navigation
        // -------------------------

        // Prevent dropdown from closing on click
        jQuery(document).on('click', '.dropdown-content', function (e) {
          e.stopPropagation();
        });

        // Disabled links
        jQuery('.navbar-nav .disabled a').on('click', function (e) {
          e.preventDefault();
          e.stopPropagation();
        });

        // Show tabs inside dropdowns
        jQuery('.dropdown-content a[data-toggle="tab"]').on('click', function (e) {
          jQuery(this).tab('show');
        });


        // ========================================
        //
        // Element controls
        //
        // ========================================


        // Reload elements
        // -------------------------

        // Panels
        jQuery('.panel [data-action=reload]').click(function (e) {
          e.preventDefault();
          var block = jQuery(this).parent().parent().parent().parent().parent();
          jQuery(block).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
              backgroundColor: '#fff',
              opacity: 0.8,
              cursor: 'wait',
              'box-shadow': '0 0 0 1px #ddd'
            },
            css: {
              border: 0,
              padding: 0,
              backgroundColor: 'none'
            }
          });

          // For demo purposes
          window.setTimeout(function () {
            jQuery(block).unblock();
          }, 2000);
        });


        // Sidebar categories
        jQuery('.category-title [data-action=reload]').click(function (e) {
          e.preventDefault();
          var block = jQuery(this).parent().parent().parent().parent();
          jQuery(block).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
              backgroundColor: '#000',
              opacity: 0.5,
              cursor: 'wait',
              'box-shadow': '0 0 0 1px #000'
            },
            css: {
              border: 0,
              padding: 0,
              backgroundColor: 'none',
              color: '#fff'
            }
          });

          // For demo purposes
          window.setTimeout(function () {
            jQuery(block).unblock();
          }, 2000);
        });


        // Light sidebar categories
        jQuery('.sidebar-default .category-title [data-action=reload]').click(function (e) {
          e.preventDefault();
          var block = jQuery(this).parent().parent().parent().parent();
          jQuery(block).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
              backgroundColor: '#fff',
              opacity: 0.8,
              cursor: 'wait',
              'box-shadow': '0 0 0 1px #ddd'
            },
            css: {
              border: 0,
              padding: 0,
              backgroundColor: 'none'
            }
          });

          // For demo purposes
          window.setTimeout(function () {
            jQuery(block).unblock();
          }, 2000);
        });


        // Collapse elements
        // -------------------------

        //
        // Sidebar categories
        //

        // Hide if collapsed by default
        jQuery('.category-collapsed').children('.category-content').hide();


        // Rotate icon if collapsed by default
        jQuery('.category-collapsed').find('[data-action=collapse]').addClass('rotate-180');


        // Collapse on click
        jQuery('.category-title [data-action=collapse]').click(function (e) {
          e.preventDefault();
          var jQuerycategoryCollapse = jQuery(this).parent().parent().parent().nextAll();
          jQuery(this).parents('.category-title').toggleClass('category-collapsed');
          jQuery(this).toggleClass('rotate-180');

          containerHeight(); // adjust page height

          jQuerycategoryCollapse.slideToggle(150);
        });


        //
        // Panels
        //

        // Hide if collapsed by default
        jQuery('.panel-collapsed').children('.panel-heading').nextAll().hide();


        // Rotate icon if collapsed by default
        jQuery('.panel-collapsed').find('[data-action=collapse]').addClass('rotate-180');


        // Collapse on click
        jQuery('.panel [data-action=collapse]').click(function (e) {
          e.preventDefault();
          var jQuerypanelCollapse = jQuery(this).parent().parent().parent().parent().nextAll();
          jQuery(this).parents('.panel').toggleClass('panel-collapsed');
          jQuery(this).toggleClass('rotate-180');

          containerHeight(); // recalculate page height

          jQuerypanelCollapse.slideToggle(150);
        });


        // Remove elements
        // -------------------------

        // Panels
        jQuery('.panel [data-action=close]').click(function (e) {
          e.preventDefault();
          var jQuerypanelClose = jQuery(this).parent().parent().parent().parent().parent();

          containerHeight(); // recalculate page height

          jQuerypanelClose.slideUp(150, function () {
            jQuery(this).remove();
          });
        });


        // Sidebar categories
        jQuery('.category-title [data-action=close]').click(function (e) {
          e.preventDefault();
          var jQuerycategoryClose = jQuery(this).parent().parent().parent().parent();

          containerHeight(); // recalculate page height

          jQuerycategoryClose.slideUp(150, function () {
            jQuery(this).remove();
          });
        });


        // ========================================
        //
        // Main navigation
        //
        // ========================================


        // Main navigation
        // -------------------------

        // Add 'active' class to parent list item in all levels
        jQuery('.navigation').find('li.active').parents('li').addClass('active');

        // Hide all nested lists
        jQuery('.navigation').find('li').not('.active, .category-title').has('ul').children('ul').addClass('hidden-ul');

        // Highlight children links
        jQuery('.navigation').find('li').has('ul').children('a').addClass('has-ul');

        // Add active state to all dropdown parent levels
        jQuery('.dropdown-menu:not(.dropdown-content), .dropdown-menu:not(.dropdown-content) .dropdown-submenu').has('li.active').addClass('active').parents('.navbar-nav .dropdown:not(.language-switch), .navbar-nav .dropup:not(.language-switch)').addClass('active');


        // Main navigation tooltips positioning
        // -------------------------

        // Left sidebar
        jQuery('.navigation-main > .navigation-header > i').tooltip({
          placement: 'right',
          container: 'body'
        });


        // Collapsible functionality
        // -------------------------

        // Main navigation
        jQuery('.navigation-main').find('li').has('ul').children('a').on('click', function (e) {
          e.preventDefault();

          // Collapsible
          jQuery(this).parent('li').not('.disabled').not(jQuery('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).toggleClass('active').children('ul').slideToggle(250);

          // Accordion
          if (jQuery('.navigation-main').hasClass('navigation-accordion')) {
            jQuery(this).parent('li').not('.disabled').not(jQuery('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).siblings(':has(.has-ul)').removeClass('active').children('ul').slideUp(250);
          }
        });


        // Alternate navigation
        jQuery('.navigation-alt').find('li').has('ul').children('a').on('click', function (e) {
          e.preventDefault();

          // Collapsible
          jQuery(this).parent('li').not('.disabled').toggleClass('active').children('ul').slideToggle(200);

          // Accordion
          if (jQuery('.navigation-alt').hasClass('navigation-accordion')) {
            jQuery(this).parent('li').not('.disabled').siblings(':has(.has-ul)').removeClass('active').children('ul').slideUp(200);
          }
        });


        // ========================================
        //
        // Sidebars
        //
        // ========================================


        // Mini sidebar
        // -------------------------

        // Toggle mini sidebar
        jQuery('.sidebar-main-toggle').on('click', function (e) {
          e.preventDefault();

          // Toggle min sidebar class
          jQuery('body').toggleClass('sidebar-xs');
        });


        // Sidebar controls
        // -------------------------

        // Disable click in disabled navigation items
        jQuery(document).on('click', '.navigation .disabled a', function (e) {
          e.preventDefault();
        });


        // Adjust page height on sidebar control button click
        jQuery(document).on('click', '.sidebar-control', function (e) {
          containerHeight();
        });


        // Hide main sidebar in Dual Sidebar
        jQuery(document).on('click', '.sidebar-main-hide', function (e) {
          e.preventDefault();
          jQuery('body').toggleClass('sidebar-main-hidden');
        });


        // Toggle second sidebar in Dual Sidebar
        jQuery(document).on('click', '.sidebar-secondary-hide', function (e) {
          e.preventDefault();
          jQuery('body').toggleClass('sidebar-secondary-hidden');
        });


        // Hide detached sidebar
        jQuery(document).on('click', '.sidebar-detached-hide', function (e) {
          e.preventDefault();
          jQuery('body').toggleClass('sidebar-detached-hidden');
        });


        // Hide all sidebars
        jQuery(document).on('click', '.sidebar-all-hide', function (e) {
          e.preventDefault();

          jQuery('body').toggleClass('sidebar-all-hidden');
        });


        //
        // Opposite sidebar
        //

        // Collapse main sidebar if opposite sidebar is visible
        jQuery(document).on('click', '.sidebar-opposite-toggle', function (e) {
          e.preventDefault();

          // Opposite sidebar visibility
          jQuery('body').toggleClass('sidebar-opposite-visible');

          // If visible
          if (jQuery('body').hasClass('sidebar-opposite-visible')) {

            // Make main sidebar mini
            jQuery('body').addClass('sidebar-xs');

            // Hide children lists
            jQuery('.navigation-main').children('li').children('ul').css('display', '');
          }
          else {

            // Make main sidebar default
            jQuery('body').removeClass('sidebar-xs');
          }
        });


        // Hide main sidebar if opposite sidebar is shown
        jQuery(document).on('click', '.sidebar-opposite-main-hide', function (e) {
          e.preventDefault();

          // Opposite sidebar visibility
          jQuery('body').toggleClass('sidebar-opposite-visible');

          // If visible
          if (jQuery('body').hasClass('sidebar-opposite-visible')) {

            // Hide main sidebar
            jQuery('body').addClass('sidebar-main-hidden');
          }
          else {

            // Show main sidebar
            jQuery('body').removeClass('sidebar-main-hidden');
          }
        });


        // Hide secondary sidebar if opposite sidebar is shown
        jQuery(document).on('click', '.sidebar-opposite-secondary-hide', function (e) {
          e.preventDefault();

          // Opposite sidebar visibility
          jQuery('body').toggleClass('sidebar-opposite-visible');

          // If visible
          if (jQuery('body').hasClass('sidebar-opposite-visible')) {

            // Hide secondary
            jQuery('body').addClass('sidebar-secondary-hidden');

          }
          else {

            // Show secondary
            jQuery('body').removeClass('sidebar-secondary-hidden');
          }
        });


        // Hide all sidebars if opposite sidebar is shown
        jQuery(document).on('click', '.sidebar-opposite-hide', function (e) {
          e.preventDefault();

          // Toggle sidebars visibility
          jQuery('body').toggleClass('sidebar-all-hidden');

          // If hidden
          if (jQuery('body').hasClass('sidebar-all-hidden')) {

            // Show opposite
            jQuery('body').addClass('sidebar-opposite-visible');

            // Hide children lists
            jQuery('.navigation-main').children('li').children('ul').css('display', '');
          }
          else {

            // Hide opposite
            jQuery('body').removeClass('sidebar-opposite-visible');
          }
        });


        // Keep the width of the main sidebar if opposite sidebar is visible
        jQuery(document).on('click', '.sidebar-opposite-fix', function (e) {
          e.preventDefault();

          // Toggle opposite sidebar visibility
          jQuery('body').toggleClass('sidebar-opposite-visible');
        });


        // Mobile sidebar controls
        // -------------------------

        // Toggle main sidebar
        jQuery('.sidebar-mobile-main-toggle').on('click', function (e) {
          e.preventDefault();
          jQuery('body').toggleClass('sidebar-mobile-main').removeClass('sidebar-mobile-secondary sidebar-mobile-opposite sidebar-mobile-detached');
        });


        // Toggle secondary sidebar
        jQuery('.sidebar-mobile-secondary-toggle').on('click', function (e) {
          e.preventDefault();
          jQuery('body').toggleClass('sidebar-mobile-secondary').removeClass('sidebar-mobile-main sidebar-mobile-opposite sidebar-mobile-detached');
        });


        // Toggle opposite sidebar
        jQuery('.sidebar-mobile-opposite-toggle').on('click', function (e) {
          e.preventDefault();
          jQuery('body').toggleClass('sidebar-mobile-opposite').removeClass('sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-detached');
        });


        // Toggle detached sidebar
        jQuery('.sidebar-mobile-detached-toggle').on('click', function (e) {
          e.preventDefault();
          jQuery('body').toggleClass('sidebar-mobile-detached').removeClass('sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-opposite');
        });


        // Mobile sidebar setup
        // -------------------------

        jQuery(window).on('resize', function () {
          setTimeout(function () {
            containerHeight();

            if (jQuery(window).width() <= 768) {

              // Add mini sidebar indicator
              jQuery('body').addClass('sidebar-xs-indicator');

              // Place right sidebar before content
              jQuery('.sidebar-opposite').insertBefore('.content-wrapper');

              // Place detached sidebar before content
              jQuery('.sidebar-detached').insertBefore('.content-wrapper');

              // Add mouse events for dropdown submenus
              jQuery('.dropdown-submenu').on('mouseenter', function () {
                jQuery(this).children('.dropdown-menu').addClass('show');
              }).on('mouseleave', function () {
                jQuery(this).children('.dropdown-menu').removeClass('show');
              });
            }
            else {

              // Remove mini sidebar indicator
              jQuery('body').removeClass('sidebar-xs-indicator');

              // Revert back right sidebar
              jQuery('.sidebar-opposite').insertAfter('.content-wrapper');

              // Remove all mobile sidebar classes
              jQuery('body').removeClass('sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-detached sidebar-mobile-opposite');

              // Revert left detached position
              if (jQuery('body').hasClass('has-detached-left')) {
                jQuery('.sidebar-detached').insertBefore('.container-detached');
              }

              // Revert right detached position
              else if (jQuery('body').hasClass('has-detached-right')) {
                jQuery('.sidebar-detached').insertAfter('.container-detached');
              }

              // Remove visibility of heading elements on desktop
              jQuery('.page-header-content, .panel-heading, .panel-footer').removeClass('has-visible-elements');
              jQuery('.heading-elements').removeClass('visible-elements');

              // Disable appearance of dropdown submenus
              jQuery('.dropdown-submenu').children('.dropdown-menu').removeClass('show');
            }
          }, 100);
        }).resize();


        // ========================================
        //
        // Other code
        //
        // ========================================


        // Plugins
        // -------------------------

        // Popover
        jQuery('[data-popup="popover"]').popover();


        // Tooltip
        jQuery('[data-popup="tooltip"]').tooltip();

      });

    }, 10);
  }
}


export class InitialPreviewConfig {
  caption?: string;
  size?: number;
  width?: string;
  type?: string;
  filetype?: string;
  url: string;
  key: number;
  downloadUrl?: string;
}

