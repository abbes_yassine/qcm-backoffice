import {StorageService} from "app/shared/services/storage.service";
import {Injectable} from "@angular/core";
import {GenericService} from "./generic.service";
import {HttpClient} from "@angular/common/http";
import {catchError} from 'rxjs/operators';
import {User} from "../models/user/user";
import {Config} from "../config";
import {Credential} from "../models/user/credential";

@Injectable()
export class UserService extends GenericService {
  loggedAdmin: User;

  userBaseUrl: string = Config.baseUrl + "/api/customer";

  constructor(private http: HttpClient, private storageService: StorageService) {
    super();
    this.loggedAdmin = <User> storageService.read(Config.userKey);
  }

  login(credentials: Credential) {
    const url = this.userBaseUrl + '/guest/login';
    return this.http.post<any>(url, credentials)
      .pipe(catchError(this.handleErrors));
  }

  isLoggedIn() {
    return this.storageService.read(Config.userKey) != null;
  }

  getLoggedUser() {
    const url = this.userBaseUrl + "/me";

    return this.http.get<any>(url)
      .pipe(catchError(this.handleErrors));
  }

  getToken() {
    return <string>this.storageService.read(Config.userTokenKey);
  }


  clear() {
    this.storageService.removeAll();
  }

  register(user: User) {
    const url = this.userBaseUrl + "/guest/register";

    return this.http.post<any>(url,
      user)
      .pipe(catchError(this.handleErrors));
  }

  storeUserAccessToken(accessToken: string) {
    this.storageService.write(Config.userTokenKey, accessToken);
  }

  storeUser(user: User) {
    this.storageService.write(Config.userKey, user);
  }
}
