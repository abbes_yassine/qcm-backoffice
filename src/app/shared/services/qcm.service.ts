import {Injectable} from "@angular/core";
import {GenericService} from "./generic.service";
import {HttpClient} from "@angular/common/http";
import {catchError} from 'rxjs/operators';
import {Config} from "../config";
import {QCM} from "../models/qcm/qcm";

@Injectable()
export class QcmService extends GenericService {

  qcmBaseUrl: string = Config.baseUrl + "/api/qcm";

  constructor(private http: HttpClient) {
    super();
  }

  getQCMByUser() {
    const url = this.qcmBaseUrl + "/user";

    return this.http.get<any>(url)
      .pipe(catchError(this.handleErrors));
  }

  addQCM(qcm: QCM) {
    const url = this.qcmBaseUrl + '/user';
    return this.http.post<any>(url, qcm)
      .pipe(catchError(this.handleErrors));
  }

  deleteQCM(qcmId: number) {
    const url = this.qcmBaseUrl + '/' + qcmId;
    return this.http.delete<any>(url)
      .pipe(catchError(this.handleErrors));
  }

  getQCMById(qcmId: number) {
    const url = this.qcmBaseUrl + "/" + qcmId;

    return this.http.get<any>(url)
      .pipe(catchError(this.handleErrors));
  }

  editQCM(newQcm: QCM) {
    const url = this.qcmBaseUrl + '/' + newQcm.id;
    return this.http.put<any>(url, newQcm)
      .pipe(catchError(this.handleErrors));
  }
}
