import {Question} from "./question";

export class QCM {
  id: number;
  title: string;
  description: string;

  nbR: number = 5;

  questions : Question[] = [];
}

