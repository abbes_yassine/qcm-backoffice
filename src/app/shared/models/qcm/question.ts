import {Response} from "./response";

export class Question {
  title: string;
  percentage: number;

  responses: Response[] = [];
}
