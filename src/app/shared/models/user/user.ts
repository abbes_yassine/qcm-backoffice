export class User {
  firstName: string;
  lastName: string;
  phone: string;
  email: string;

  cpassword: string;
  password: string;

  authorities: string[] = ['USER']
}
