import {User} from "./user";

export class UserLoginResponse {
  user: User;
  accessToken: string;
}
