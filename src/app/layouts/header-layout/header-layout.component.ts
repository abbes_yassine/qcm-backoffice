import {Component, OnInit} from '@angular/core';
import {StorageService} from "../../shared/services/storage.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header-layout',
  templateUrl: './header-layout.component.html',
  styleUrls: ['./header-layout.component.css']
})
export class HeaderLayoutComponent implements OnInit {

  isNavCollapsed = false;

  constructor(private storageService: StorageService,
              private router: Router) {

  }

  ngOnInit() {

  }

  getUserImg() {
    return 'assets/images/placeholder.jpg';
  }

  switchNavCollapseMode() {
    this.isNavCollapsed = !this.isNavCollapsed
  }

  logout() {
    this.storageService.removeAll();
    this.router.navigateByUrl("/login");
  }

}
