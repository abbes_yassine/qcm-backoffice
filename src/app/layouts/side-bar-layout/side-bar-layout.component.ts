import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Utils} from "../../shared/utils";

@Component({
  selector: 'app-side-bar-layout',
  templateUrl: './side-bar-layout.component.html',
  styleUrls: ['./side-bar-layout.component.css']
})
export class SideBarLayoutComponent implements OnInit {
  components: NavigationMain[] = [];

  constructor(private router: Router) {
  }

  ngOnInit() {


    this.components = [
      {
        name: "Gestion des QCM",
        icon: "icon-address-book",
        hidden: false,
        childrens: [
          {
            name: "Liste des QCM",
            url: "/manage-qcm/list"
          }, {
            name: "Ajout QCM",
            url: "/manage-qcm/add"
          }
        ]
      }
    ];

    Utils.initJsProject();

    this.changeActiveUrl(this.router.url);
  }

  getUserImg() {
    return 'assets/images/placeholder.jpg';
  }



  changeActiveUrl(url: string) {
    this.components.forEach(
      component => {
        component.active = "";
        if (url.indexOf(component.url) !== -1) {
          component.active = "active";
        }
        if (component.childrens) {
          component.childrens.forEach(
            child => {
              child.active = "";
              if (url.indexOf(child.url) !== -1) {
                child.active = "active";
              }
            }
          )
        }
      }
    )
  }

  goUrl(url: string) {
    console.log("url + ", url);
    if (url) {
      this.router.navigate([url]);
    }
  }

}

export class NavigationMain {
  public name: string;
  public icon: string;
  public active?: string;
  public childrens?: ChildrenNavigation[] = [];
  public url?: string;
  public hidden?: boolean;
  public notification?: number;

}

export class ChildrenNavigation {
  public name: string;
  public active?: string;
  public url: string;
  public hidden?: boolean;
  public notification?: number;
  public action?: any;
}
