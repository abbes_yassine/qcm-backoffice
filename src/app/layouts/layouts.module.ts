import {NgModule} from '@angular/core';
import {FullLayoutComponent} from "./full-layout.component";
import {HeaderLayoutComponent} from './header-layout/header-layout.component';
import {FooterLayoutComponent} from './footer-layout/footer-layout.component';
import {SharedModule} from "../shared/shared.module";
import {AppRoutingModule} from "../app.routing";
import {SideBarLayoutComponent} from './side-bar-layout/side-bar-layout.component';
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    FullLayoutComponent,
    HeaderLayoutComponent,
    FooterLayoutComponent,
    SideBarLayoutComponent
  ],
  imports: [
    RouterModule,
    SharedModule
  ]
})
export class LayoutsModule {
}
