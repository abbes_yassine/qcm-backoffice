import {Component, OnInit} from '@angular/core';
import {QCM} from "../../shared/models/qcm/qcm";
import {Response} from "../../shared/models/qcm/response";
import {QcmService} from "../../shared/services/qcm.service";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";

declare let swal: any;

@Component({
  selector: 'app-edit-qcm',
  templateUrl: './edit-qcm.component.html',
  styleUrls: ['./edit-qcm.component.css']
})
export class EditQcmComponent implements OnInit {

  qcm: QCM;
  busy: Subscription;
  qcmId: number;

  constructor(private qcmService: QcmService,
              private router: Router,
              private route: ActivatedRoute) {
    this.qcm = new QCM();
    this.qcmId = parseInt(this.route.snapshot.paramMap.get("qcmId"), 0);
  }

  ngOnInit() {

    if (this.qcmId) {
      this.getQCMById(this.qcmId);
    } else {
      this.initQuestion(0);
    }
  }

  private initQuestion(index: number) {
    if (index === 0) this.qcm.questions = [];

    this.qcm.questions.splice(index, 0, {
      title: "Q" + (index + 1),
      percentage: 0,
      responses: this.initResponses(this.qcm.nbR)
    });
  }

  private initResponses(nbR: number) {
    const tmpArray: Response[] = [];
    for (let i = 0; i < nbR; i++) {
      tmpArray.push({
        title: String.fromCharCode(65 + i),
        percentage: 0
      });
    }
    return tmpArray;
  }

  getQuestionPonderation(index: number) {
    if (!this.qcm.questions[index]) return 0;
    let sum = 0;
    this.qcm.questions[index].responses
      .forEach(
        response => {
          sum += response.percentage;
        }
      )
    return sum;
  }

  addQuestion(index: number) {
    if (!this.isGrilleValid()) {
      return;
    }
    this.initQuestion(index + 1);
  }

  getQCMPonderation() {
    if (!this.qcm.questions) return 0;
    let sum = 0;
    this.qcm.questions.forEach(
      question => {
        sum += question.percentage
      }
    );
    return sum;
  }

  isGrilleValid() {
    for (let i = 0; i < this.qcm.questions.length; i++) {
      if (this.getQuestionPonderation(i) !== 100) {
        return false;
      }
    }
    return true;
  }


  submitQCM() {

    if (!this.isGrilleValid() || this.getQCMPonderation() !== 100) {
      swal('Warning', 'Vous devez remplir les champs vides !!', 'warning');
      return;
    }
    console.log(this.qcm);

    if (!this.qcm.id) {
      this.addQCM();
    } else {
      this.editQCM();
    }

  }

  editQCM() {
    this.busy = this.qcmService
      .editQCM(this.qcm)
      .subscribe(
        (data) => {
          swal('Succées', "Modification QCM avec succée", 'success');
          this.router.navigate(['/manage-qcm/list']);
        },
        (error) => {

        }
      )
  }

  addQCM() {
    this.busy = this.qcmService.addQCM(this.qcm)
      .subscribe(
        (data) => {
          swal('Succées', "Ajout QCM avec succée", 'success');
          this.router.navigate(['/manage-qcm/list']);
        },
        (error) => {

        }
      )
  }

  private getQCMById(qcmId: number) {
    this.busy = this.qcmService
      .getQCMById(qcmId)
      .subscribe(
        (data) => {
          this.qcm = data;
          this.qcm.nbR = this.qcm.questions[0].responses.length;
        },
        (error) => {

        }
      )
  }
}
