import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListQcmComponent} from "./list-qcm/list-qcm.component";
import {EditQcmComponent} from "./edit-qcm/edit-qcm.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ListQcmComponent
      },
      {
        path: 'add',
        component: EditQcmComponent
      },
      {
        path: ':qcmId/edit',
        component: EditQcmComponent
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageQcmRoutingModule {
}
