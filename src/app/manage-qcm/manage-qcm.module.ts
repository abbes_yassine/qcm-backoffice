import {NgModule} from '@angular/core';

import {ManageQcmRoutingModule} from './manage-qcm-routing.module';
import {ListQcmComponent} from './list-qcm/list-qcm.component';
import {EditQcmComponent} from './edit-qcm/edit-qcm.component';
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [ListQcmComponent, EditQcmComponent],
  imports: [
    SharedModule,
    ManageQcmRoutingModule
  ]
})
export class ManageQcmModule {
}
