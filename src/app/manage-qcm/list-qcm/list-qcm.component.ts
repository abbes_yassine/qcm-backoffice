import {Component, OnInit} from '@angular/core';
import {QCM} from "../../shared/models/qcm/qcm";
import {Subscription} from "rxjs";
import {QcmService} from "../../shared/services/qcm.service";
import {Utils} from "../../shared/utils";


declare let swal: any;

@Component({
  selector: 'app-list-qcm',
  templateUrl: './list-qcm.component.html',
  styleUrls: ['./list-qcm.component.css']
})
export class ListQcmComponent implements OnInit {

  qcms: QCM[] = [];
  busy: Subscription;

  constructor(private qcmService: QcmService) {
  }

  ngOnInit() {
    this.loadAllQCM();
  }


  deleteQCM(index: number) {
    const baseContext = this;

    swal({
        title: "Vous êtes sûr?",
        text: "Ce QCM va être supprimer définitivement!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#EF5350",
        confirmButtonText: "Oui, supprimer!",
        cancelButtonText: "Non, annuler!",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function (isConfirm) {
        if (isConfirm) {
          baseContext.deleteQCMI(index)
        } else {
          swal("Annulé", "Vous avez annulé cette action", "error");
        }
      });


  }

  private deleteQCMI(index: number) {
    const baseContext = this;
    baseContext.busy = baseContext.qcmService
      .deleteQCM(baseContext.qcms[index].id)
      .subscribe(
        (data) => {
          baseContext.qcms.splice(index, 1);
          Utils.initializeDataTables('table-qcm', 10, 6);
          swal("Supprimé!", "Ce QCM est supprimé.", "success");
        },
        (error) => {
        }
      )
  }

  private loadAllQCM() {
    this.busy = this.qcmService
      .getQCMByUser()
      .subscribe(
        (data: QCM[]) => {
          this.qcms = data;
          Utils.initializeDataTables('table-qcm', 10, 6);
        },
        (error) => {
        }
      )
  }
}
