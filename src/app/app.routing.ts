import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {LoginComponent} from "./manage-user/login/login.component";
import {FullLayoutComponent} from "./layouts/full-layout.component";
import {NotFoundComponent} from "./error/not-found/not-found.component";
import {CanActivateViaAuthGuard} from "./shared/services/guards/auth-guard.service";
import {RegisterComponent} from "./manage-user/register/register.component";
import {ManageQcmModule} from "./manage-qcm/manage-qcm.module";


export function loadManageQCMModule() {
  return ManageQcmModule
}

export const routes: Routes = [
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [CanActivateViaAuthGuard],
    children: [
      {
        path: 'manage-qcm',
        loadChildren: loadManageQCMModule
      }
    ],
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
