import {Component, OnInit} from '@angular/core';
import {User} from "../../shared/models/user/user";
import {UserService} from "../../shared/services/user.service";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";


declare var swal: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  busy: Subscription;
  user: User;

  constructor(private userService: UserService,
              private router: Router) {
    this.user = new User();
  }

  ngOnInit() {
  }


  registerUser() {

    this.busy = this.userService
      .register(this.user)
      .subscribe(
        (data) => {
          swal('Succées', 'Un mail de confirmation est envoyée', 'success');
          this.router.navigate(['/login']);
        },
        (error) => {
          swal('Erreur', 'Une erreur est survenue', 'error');
        }
      )

  }
}
