import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {UserService} from "../../shared/services/user.service";
import {Subscription} from "rxjs";
import {Credential} from "../../shared/models/user/credential";
import {UserLoginResponse} from "../../shared/models/user/user-login-response";

declare let jQuery: any;
declare let swal: any;

@Component({
  templateUrl: './login.component.html',
  styleUrls: [],
})
export class LoginComponent implements OnInit {

  busy: Subscription;

  credentials: Credential;

  ngOnInit() {
  }


  constructor(private userService: UserService,
              private router: Router) {
    this.credentials = new Credential();
  }

  loginSubmit() {
    this.busy = this.userService
      .login(this.credentials)
      .subscribe(
        (data: UserLoginResponse) => {
          this.userService.storeUserAccessToken(data.accessToken);
          this.userService.storeUser(data.user);
          this.router.navigate(['/']);
        },
        (error) => {
          swal('Erreur', 'Une erreur est survenue', 'error');
        }
      )
  }
}


