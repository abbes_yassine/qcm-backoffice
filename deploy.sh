app="app-qcm-backoffice"
image="qcm-backoffice"
host="qcm-management.vayetek.com"
email="vayetek@gmail.com"
network="nginx-proxy"
docker stop "$app" || true && docker rm -f "$app" || true
docker rmi "$image"|| true
docker build -t "$image" .
docker run -p 4404:80 -d -e VIRTUAL_HOST="$host" -e LETSENCRYPT_HOST="$host" -e LETSENCRYPT_EMAIL="$email" --network="$network" --name "$app" "$image"
